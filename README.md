### **Description**
Uživatel zadá vždy datum k danému datu hodnoty plynu, elektriky a vody. Program je zapíše do TXT souboru a spočítá spotřebu za den + podle tarifu za rok i cenu... Dále tam bude nabídka, že si může pomocí datumu vyhledat dané hodnoty a program mu to znova spočítá... a vypíše do HTML....   

Tarify budou konstanty v programu. Budou 3 - zvlášť pro el, plyn i vodu.... U6ivatelem zadané hodnoty + datumy se vždy uloží do stejného txt souboru: plyn.txt voda.txt elektrika.txt  

Příklad: Na jednom řádku txt souboru bude například: 1.1.2012 457  

a) výpočet spotřeby za jeden den (hodnota současná - hodnota minulá)  
b) výpočet ceny za jeden den (hodnota která vyjde v a) * konstanta (tarif - pro plyn / vodu / elektriku)  

Uživatel by měl zadat:
a) datum - zapíše se do TXT souboru vždy na stejný řádek jako hodnota pro dané datum (jenom pro orientaci v TXT)
b) jednotlivé hodnoty

Mělo by se vytvořit:
a) 3 txt soubory s nápisem elektrika, voda, plyn
b) html výstup ve kterém budou napsané výpočty a z jakých hodnot se vycházelo

---
### **Technology**
C++

---
### **Year**
2011

---
### **Screenshot**
![](./README/spotreba.png)
