/*
 * Soubor:  energie.cpp
 * Datum:   2011/12/29
 * Autor:   
 * Projekt do ZP
 */

//pripojeni knihovny jazyka c++ pro vstup/vystupni operace
#include <iostream>

using namespace std;

//konstanty pro cenu elektriky, vody a plynu, cena je za jednu jednotku
const int const_elektrika = 20;
const int const_voda = 10;
const int const_plyn = 30;


/** \brief cte a kontroluje parametry prikazoveho radku
 *
 * Pokud je zadan parametr "-h", 
 * pak se vola funkce pro vypis napovedy a program uspesne skonci.
 * Poud je zadan 1 jiny parametr nebo vice, pak program konci s chybou.
 * Pokud neni zadan zadny parametr, pak funkce nic nedela.
 *
 * @param argc udava pocet zadanych parametru se kterymi je program spusten
 * @param *argv[] je pole ukazatelu ukazujicich na pamet ve ktere je ulozen obsah parametru prikazoveho radku
 */
void kontrola_parametru(int argc, char *argv[])
{
	//pokud neni zadan zadny parametr, nebo prave jeden "--h", pak se vytiskne napoveda
	if((argc == 2) && (strcmp("--h", argv[1]) == 0))
	{
		cout << "Napoveda:" << endl;
		cout << "Kdyz se zada -h, tak se vytiskne napoveda" << endl;
		cout << "Kdyz se zada jeden 1 jiny parametr nez \"-h\" nebo vice, pak program konci s chybou" << endl;
		cout << "Kdyz se nezada zadny parametr, pak se program pta, jestli ma zapsat novou hodnotu," <<endl;
		cout << "Vypo��tat cenu za posledn� den, nebo vypsat historii a to vse vypsat do HTML souboru" << endl;
		system("PAUSE");
		exit(EXIT_SUCCESS);
	}

	//pokud je zadan jiny pocet parametru, pak se jedna o chybu
	else if (argc > 1)
	{
		cout << "zadano moc parametru, program se spousti s parametrem \"-h\" nebo s zadnym" << endl;
		exit(EXIT_FAILURE);
	}
}

/** \brief zapisuje zadane hodnoty do prislusnych souboru
 *
 * Vytvori, nebo pokud existuji tak otevre, soubory elektrika.txt, voda.txt, plyn.txt
 * a zapise do nich datum a vedle nej prislusnou hodnotu kterou zadal uzivatel. Po te
 * se soubory uzavrou
 *
 * @param dat datum ktere je platne pro prave zapisovanou hodnotu
 * @param el uzivatelem zadana hodnota elektriky k danemu datu
 * @param voda uzivatelem zadana hodnota vody k danemu datu
 * @param plyn uzivatelem zadana hodnota plynu k danemu datu
 *
 */
void zapis_hodnot(char* dat, int el, int voda, int plyn)
{
	//soubor pro zapis hodnot elektriky
	FILE *soubor_elektrika;
	//otevreni souboru
	soubor_elektrika = fopen("elektrika.txt", "a+");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_elektrika == NULL)
	{
		cout << "chyba pri otvirani souboru elektrika.txt" << endl;
		//je potreba pred skonceni uvolnit pamet
		free(dat);
		exit(EXIT_FAILURE);
	}

	//soubor pro zapis hodnot vody
	FILE *soubor_voda;
	//otevreni souboru
	soubor_voda = fopen("voda.txt", "a+");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_voda == NULL)
	{
		cout << "chyba pri otvirani souboru voda.txt" << endl;
		//je ptoreba zavrit otevrene soubory
		fclose(soubor_elektrika);
		//je potreba pred skonceni uvolnit pamet
		free(dat);
		exit(EXIT_FAILURE);
	}

	//soubor pro zapis hodnot plynu
	FILE *soubor_plyn;
	//otevreni souboru
	soubor_plyn = fopen("plyn.txt", "a+");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_plyn == NULL)
	{
		cout << "chyba pri otvirani souboru plyn.txt" << endl;
		//je potreba zavrit otevrene soubory
		fclose(soubor_elektrika);
		fclose(soubor_voda);
		//je potreba pred skonceni uvolnit pamet
		free(dat);
		exit(EXIT_FAILURE);			
	}

	//zapis vsech hodnot az po otevreni vsech souboru je zde proto, aby se zapsalo bud vsechno nebo nic
	//zapis probiha do souboru a pro kontrolu i na stdout
	cout << "Probiha zapis do souboru" <<endl;
	cout << "elektrika: " << dat << " " << el << endl;
	fprintf(soubor_elektrika, "%s %i\r\n", dat, el);
	cout << "voda:      " << dat << " " << voda << endl;
	fprintf(soubor_voda, "%s %i\r\n", dat, voda);
	cout << "plyn:      " << dat << " " << plyn << endl;
	fprintf(soubor_plyn, "%s %i\r\n", dat, plyn);

	//uzavreni vsech 3 souboru
	fclose(soubor_elektrika);
	fclose(soubor_voda);
	fclose(soubor_plyn);
}

/** \brief cte a zpracovava hodnoty z prislusnych souboru
 *
 * Otevre, soubory elektrika.txt, voda.txt, plyn.txt a precte z nich posledni 2 udaje.
 * Z techto udaju se vzdy spocita rozdil, ktery se ulozi vzdy do prislusne promenne.
 * na konec je potreba soubory uzavrit
 *
 * @param dat naalokovana pamet, kterou je potreba v pripade chyby uvolnit
 * @param el zde bude po skonceni funkce pocet jednotek mezi 2 poslednimy zaznamy v souboru elektrika.txt
 * @param voda zde bude po skonceni funkce pocet jednotek mezi 2 poslednimy zaznamy v souboru voda.txt
 * @param plyn zde bude po skonceni funkce pocet jednotek mezi 2 poslednimy zaznamy v souboru plyn.txt
 */
void zpracovani_hodnot(char* dat, char* datum1, char* datum2, int *el, int *voda, int *plyn)
{
	//mam 2 pomocne retezce ve kterych po precteni souboru zustanou posledni 2 radky ze kterych
	//se "vytahnou" stavy, ulozi do promenne na kterou ukazuje prislusny parametr teto funkce. 
	//Stejnym zpusobem se zpracuji vsechny 3 soubory
	//cislo 30 udava maximalni delku radku, z toho prvnich 11 pripada na datum a mezeru,
	//zbylych 19 pak znaci maximalni pocet cislic cisla, ktere udava dany stav (elektriky/vody/plynu)
	char pomocny_retezec1[30];
	char pomocny_retezec2[30];

	//soubor pro cteni hodnot elektriky
	FILE *soubor_elektrika;
	//otevreni souboru
	soubor_elektrika = fopen("elektrika.txt", "r");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_elektrika == NULL)
	{
		cout << "chyba pri otvirani souboru elektrika.txt" << endl;
		//je potreba pred skonceni uvolnit pamet
		free(dat);
		exit(EXIT_FAILURE);
	}

	//soubor pro cteni hodnot vody
	FILE *soubor_voda;
	//otevreni souboru
	soubor_voda = fopen("voda.txt", "r");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_voda == NULL)
	{
		cout << "chyba pri otvirani souboru voda.txt" << endl;
		//je ptoreba zavrit otevrene soubory
		fclose(soubor_elektrika);
		//je potreba pred skonceni uvolnit pamet
		free(dat);
		exit(EXIT_FAILURE);
	}

	//soubor pro cteni hodnot plynu
	FILE *soubor_plyn;
	//otevreni souboru
	soubor_plyn = fopen("plyn.txt", "r");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_plyn == NULL)
	{
		cout << "chyba pri otvirani souboru plyn.txt" << endl;
		//je potreba zavrit otevrene soubory
		fclose(soubor_elektrika);
		fclose(soubor_voda);
		//je potreba pred skonceni uvolnit pamet
		free(dat);
		exit(EXIT_FAILURE);			
	}

	//prochazeni souboru v cyklu a ukladani jednotlivych radku stridave do retezc pomocny_retezec1 a
	//pomocny_retezec2 az do konce souboru
	while(fgets(pomocny_retezec1, 30, soubor_elektrika) != NULL)
	{
		fgets(pomocny_retezec2, 30, soubor_elektrika);
	}
	//preskoceni vsech cisel ktere udavaji datum a zapsani pouze cisel udavajicich stav
	*el = atoi(&pomocny_retezec2[11]) - atoi(&pomocny_retezec1[11]);
	//zajisteni kladneho vysledku
	//prirazeni spravneho datumu - pozna se podle toho jestli je soucasny vysledek kladny nebo ne
	if(*el < 0)
	{
		*el *= -1;
		for(int i = 0; i < 11; i++)
		{
			datum1[i] = pomocny_retezec2[i];
			datum2[i] = pomocny_retezec1[i];
		}
	}
	else
	{
		for(int i = 0; i < 11; i++)
		{
			datum1[i] = pomocny_retezec1[i];
			datum2[i] = pomocny_retezec2[i];
		}
	}

	//prochazeni souboru v cyklu a ukladani jednotlivych radku stridave do retezc pomocny_retezec1 a
	//pomocny_retezec2 az do konce souboru
	while(fgets(pomocny_retezec1, 30, soubor_voda) != NULL)
	{
		fgets(pomocny_retezec2, 30, soubor_voda);
	}
	//preskoceni vsech cisel ktere udavaji datum a zapsani pouze cisel udavajicich stav
	*voda = atoi(&pomocny_retezec2[11]) - atoi(&pomocny_retezec1[11]);
	//zajisteni kladneho vysledku
	if(*voda < 0)
	{
		*voda *= -1;
	}

	//prochazeni souboru v cyklu a ukladani jednotlivych radku stridave do retezc pomocny_retezec1 a
	//pomocny_retezec2 az do konce souboru
	while(fgets(pomocny_retezec1, 30, soubor_plyn) != NULL)
	{
		fgets(pomocny_retezec2, 30, soubor_plyn);
	}
	//preskoceni vsech cisel ktere udavaji datum a zapsani pouze cisel udavajicich stav
	*plyn = atoi(&pomocny_retezec2[11]) - atoi(&pomocny_retezec1[11]);
	//zajisteni kladneho vysledku
	if(*plyn < 0)
	{
		*plyn *= -1;
	}


	//vypis na stdout
	cout << "Spotreba elektriky od ";
	for(int i = 0; i < 11; i++)
	{
		cout << datum1[i];
	}
	cout << "do ";
	for(int i = 0; i < 11; i++)
	{
		cout << datum2[i];
	}
	cout << "je " << *el << " jednotek. " << endl;
	cout << "Pri cene " << const_elektrika << "Kc za jednotku to dela " << (*el * const_elektrika) << "Kc." << endl;

	cout << "Spotreba vody od ";
	for(int i = 0; i < 11; i++)
	{
		cout << datum1[i];
	}
	cout << "do ";
	for(int i = 0; i < 11; i++)
	{
		cout << datum2[i];
	}
	cout << "je " << *voda << " jednotek. " << endl;
	cout << "Pri cene " << const_voda << "Kc za jednotku to dela " << (*voda * const_voda) << "Kc." << endl;

	cout << "Spotreba plynu od ";
	for(int i = 0; i < 11; i++)
	{
		cout << datum1[i];
	}
	cout << "do ";
	for(int i = 0; i < 11; i++)
	{
		cout << datum2[i];
	}
	cout << "je " << *plyn << " jednotek. " << endl;
	cout << "Pri cene " << const_plyn << "Kc za jednotku to dela " << (*plyn * const_plyn) << "Kc." << endl;


	//uzavreni vsech 3 souboru
	fclose(soubor_elektrika);
	fclose(soubor_voda);
	fclose(soubor_plyn);
}

/** \brief vypis vysledku v jazyce html
 *
 * Zapise do souboru
 *
 * @param *soubor ukazatel na soubor do ktereho chceme HTML kod zapsat
 * @param dat datum ktere je platne pro prave zapisovanou hodnotu
 * @param el uzivatelem zadana hodnota elektriky k danemu datu
 * @param voda uzivatelem zadana hodnota vody k danemu datu
 * @param plyn uzivatelem zadana hodnota plynu k danemu datu
 * 
 */
void vypis_1 (FILE *soubor, char* dat, int el, int voda, int plyn)
{	
	fprintf(soubor, "<HTML>\r\n");
	fprintf(soubor, "<HEAD>\r\n");
	fprintf(soubor, "     <TITLE>ZP - Elektrika, voda, plyn</TITLE>\r\n");
	fprintf(soubor, "</HEAD>\r\n");
	fprintf(soubor, "<BODY BGCOLOR=\"#00B0F0\">\r\n");
	fprintf(soubor, "<CENTER> <H1> Elektrika, voda, plyn </H1>\r\n");
	fprintf(soubor, "<H3>  <br>\r\n");

	fprintf(soubor, "Do souboru elektrika.txt byly zaps�ny �daje: %s %i </br>\r\n", dat, el);
	fprintf(soubor, "Do souboru      voda.txt byly zaps�ny �daje: %s %i </br>\r\n", dat, voda);
	fprintf(soubor, "Do souboru      plyn.txt byly zaps�ny �daje: %s %i </br>\r\n", dat, plyn);

	fprintf(soubor, "</CENTER> \r\n");
	fprintf(soubor, "<H6>tato str�nka je vygenerov�na programem <i>energie.cpp</i> <br>\r\n");
	fprintf(soubor, "</BODY>\r\n");
	fprintf(soubor, "<HTML>\r\n");
} 

/** \brief vypis vysledku v jazyce html
 *
 * Zapise do souboru
 *
 * @param *soubor ukazatel na soubor do ktereho chceme HTML kod zapsat
 * @param datum1 datum od ktereho jsou data pocitana
 * @param datum1 datum do ktereho jsou data pocitana
 * @param el rozdil jednotek spotrebovane elektriky v obdobi mezi datum1 a datum2
 * @param voda rozdil jednotek spotrebovane vody v obdobi mezi datum1 a datum2
 * @param plyn rozdil jednotek spotrebovaneho plynu v obdobi mezi datum1 a datum2
 * 
 */
void vypis_2 (FILE *soubor, char* datum1, char* datum2, int el, int voda, int plyn)
{	
	fprintf(soubor, "<HTML>\r\n");
	fprintf(soubor, "<HEAD>\r\n");
	fprintf(soubor, "     <TITLE>ZP - Elektrika, voda, plyn</TITLE>\r\n");
	fprintf(soubor, "</HEAD>\r\n");
	fprintf(soubor, "<BODY BGCOLOR=\"#00B0F0\">\r\n");
	fprintf(soubor, "<CENTER> <H1> Elektrika, voda, plyn </H1>\r\n");

	fprintf(soubor, "Spotreba elektriky od <i>");
	for(int i = 0; i < 11; i++)
	{
		fprintf(soubor, "%c", datum1[i]);
	}
	fprintf(soubor, "</i>do <i>");
	for(int i = 0; i < 11; i++)
	{
		fprintf(soubor, "%c", datum2[i]);
	}
	fprintf(soubor, "</i>je <b>%i</b> jednotek. ", el);
	fprintf(soubor, "Pri cene %i Kc za jednotku to dela <b>%i</b> Kc. <br> \r\n", const_elektrika, (el * const_elektrika));

	fprintf(soubor, "Spotreba vody od <i>");
	for(int i = 0; i < 11; i++)
	{
		fprintf(soubor, "%c", datum1[i]);
	}
	fprintf(soubor, "</i>do <i>");
	for(int i = 0; i < 11; i++)
	{
		fprintf(soubor, "%c", datum2[i]);
	}
	fprintf(soubor, "</i>je <b>%i</b> jednotek. ", voda);
	fprintf(soubor, "Pri cene %i Kc za jednotku to dela <b>%i</b> Kc. <br> \r\n", const_voda, (voda * const_voda));

	fprintf(soubor, "Spotreba plynu od <i>");
	for(int i = 0; i < 11; i++)
	{
		fprintf(soubor, "%c", datum1[i]);
	}
	fprintf(soubor, "</i>do <i>");
	for(int i = 0; i < 11; i++)
	{
		fprintf(soubor, "%c", datum2[i]);
	}
	fprintf(soubor, "</i>je <b>%i</b> jednotek. ", plyn);
	fprintf(soubor, "Pri cene %i Kc za jednotku to dela <b>%i</b> Kc. <br> \r\n", const_plyn, (plyn * const_plyn));


	fprintf(soubor, "</CENTER> \r\n");
	fprintf(soubor, "<H6>tato str�nka je vygenerov�na programem <i>energie.cpp</i> <br>\r\n");
	fprintf(soubor, "</BODY>\r\n");
	fprintf(soubor, "<HTML>\r\n");
} 

void vypis_3(FILE *soubor)
{
	//cislo 30 udava maximalni delku radku, z toho prvnich 11 pripada na datum a mezeru,
	//zbylych 19 pak znaci maximalni pocet cislic cisla, ktere udava dany stav (elektriky/vody/plynu)
	char radek[30];

	//soubor pro cteni hodnot elektriky
	FILE *soubor_elektrika;
	//otevreni souboru
	soubor_elektrika = fopen("elektrika.txt", "r");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_elektrika == NULL)
	{
		cout << "chyba pri otvirani souboru elektrika.txt" << endl;
		exit(EXIT_FAILURE);
	}

	//soubor pro cteni hodnot vody
	FILE *soubor_voda;
	//otevreni souboru
	soubor_voda = fopen("voda.txt", "r");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_voda == NULL)
	{
		cout << "chyba pri otvirani souboru voda.txt" << endl;
		//je ptoreba zavrit otevrene soubory
		fclose(soubor_elektrika);
		exit(EXIT_FAILURE);
	}

	//soubor pro cteni hodnot plynu
	FILE *soubor_plyn;
	//otevreni souboru
	soubor_plyn = fopen("plyn.txt", "r");
	//kontrola, jestli se podarilo soubor spravne otevrit
	if (soubor_plyn == NULL)
	{
		cout << "chyba pri otvirani souboru plyn.txt" << endl;
		//je potreba zavrit otevrene soubory
		fclose(soubor_elektrika);
		fclose(soubor_voda);
		exit(EXIT_FAILURE);			
	}

	//vypis historie na stdout
	cout << "Vypis historie zaznamu elektriky:" << endl;
	while(fgets(radek, 30, soubor_elektrika) != NULL)
	{
		printf("%s", radek);
	}

	cout << endl << "Vypis historie zaznamu vody:" << endl;
	while(fgets(radek, 30, soubor_voda) != NULL)
	{
		printf("%s", radek);
	}

	cout << endl << "Vypis historie zaznamu plynu:" << endl;
	while(fgets(radek, 30, soubor_plyn) != NULL)
	{
		printf("%s", radek);
	}

	//vraceni ukazatelu ve vsech souborech zpet na zacatek, abychom mohli soubory cist znovu
	rewind(soubor_elektrika);
	rewind(soubor_voda);
	rewind(soubor_plyn);

	//------------------------------------vypis historie do html------------------------------------
	fprintf(soubor, "<HTML>\r\n");
	fprintf(soubor, "<HEAD>\r\n");
	fprintf(soubor, "     <TITLE>ZP - Elektrika, voda, plyn</TITLE>\r\n");
	fprintf(soubor, "</HEAD>\r\n");
	fprintf(soubor, "<BODY BGCOLOR=\"#00B0F0\">\r\n");
	fprintf(soubor, "<CENTER> <H1> Elektrika, voda, plyn </H1>\r\n\r\n");

	fprintf(soubor, "<table cellspacing = 5 border = 1>\r\n");
	//prvni radek tabulky
	//prvni sloupec tabulky
	fprintf(soubor, "<tr>\r\n<td align = center, valign = middle> <H3><center>ELEKTRIKA</center> </H3> </td>\r\n");
	//druhy sloupec tabulky
	fprintf(soubor, "<td align = center, valign = middle> <H3><center>VODA</center> </H3> </td>\r\n");
	//treti sloupec tabulky
	fprintf(soubor, "<td align = center, valign = middle> <H3><center>PLYN</center> </H3> </td>\r\n</tr>\r\n");
	

	//druhy radek tabulky
	//prvni sloupec tabulky
	fprintf(soubor, "<tr><td>\r\n");
	while(fgets(radek, 30, soubor_elektrika) != NULL)
	{
		fprintf(soubor, "%s<br>", radek);
	}
	fprintf(soubor, "\r\n</td><td>\r\n");
	//druhy sloupec tabulky
	while(fgets(radek, 30, soubor_voda) != NULL)
	{
		fprintf(soubor, "%s<br>", radek);
	}
	fprintf(soubor, "\r\n</td><td>\r\n");
	//treti sloupec tabulky
	while(fgets(radek, 30, soubor_plyn) != NULL)
	{
		fprintf(soubor, "%s<br>", radek);
	}
	fprintf(soubor, "\r\n</td></tr>\r\n");

	fprintf(soubor, "</CENTER> \r\n");
	fprintf(soubor, "<H6>tato str�nka je vygenerov�na programem <i>energie.cpp</i> <br>\r\n");
	fprintf(soubor, "</BODY>\r\n");
	fprintf(soubor, "<HTML>\r\n");


	//------------------------------------vypis historie do html------------------------------------

	//uzavreni vsech 3 souboru
	fclose(soubor_elektrika);
	fclose(soubor_voda);
	fclose(soubor_plyn);
}


int main(int argc,char** argv)
{
	//nejdrive ze vseho se provede kontrola parametru, jestli se ma vypsat napoveda,
	//jestli ma program probehnout, nebo jestli jsou zadany chybne parametry a ma koncit s chybou
	kontrola_parametru(argc, argv);
	
	//do promenne operace se ulozi uzivatelova volba jak ma program pokracovat.
	int operace = 0;

	//promenne pro uchovani hodnot elektriky, vody a plynu
	int prom_elektrika = 0;
	int prom_voda = 0;
	int prom_plyn = 0;

	//cyklus konci, kdyz uzivatel zada, ze ma program skoncit tim, ze bude do promenne operace zapsana hodnota 4
	while(operace != 4)
	{
		//vymazani textu - slouzi pro prehlednost
		system("cls");
		cout << endl;
		cout << "Zadej cislo a potvrd jej enterem podle toho, co chces udelat:" << endl;
		cout << endl << endl;
		cout << "  1 ... Zadani hodnot, ktere se zapisi do jednotlivych souboru" << endl;
		cout << "  2 ... spocita se spotreba elektriky, vody a plynu podle tarifu" << endl;
		cout << "  3 ... vypise se historie vsech hodnot, ktere byly do tohoto programu zapsany" << endl;
		cout << "  4 ... program skonci" << endl;
		cout << endl;

		cout << "Volba: ";
		cin >> operace;
		
		//pokud je zadano jine cislo nez v rozsahu 1 az 4, pak je vytistena chyba a program konci
		//je to proto, protoze napriklad pri zadani znaku '.' by se bez teto podminky program zacyklil
		if((operace < 1) || (operace > 4))
		{
			cout << "chyba, neplatna operace - priste zadej cislo v rozsahu 1 az 4" << endl;
			system("PAUSE");
			exit(EXIT_FAILURE);
		}

		//ukazatel na dynymicky alokovanou pamet pro uchovani zadaneho data - pouzito pro operaci 1 a vypis vysledku 
		char *datum;
		//2 pozice pro den + 1 tecka + 2 pozice pro mesic + 1 tecka + 4 pozice pro rok + 1 pozice pro konec retezce = 11 potrebnych hodnot
		datum = (char *) malloc(11);

		//vynulovani pameti
		for(int i = 0; i < 11; i++)
		{
			datum[i] = 0;
		}

		if(operace == 1)
		{

			//zapsani uzivatelem zadanych hodnot do prislusnych promennych
			cout << "Zadej datum ve tvaru dd.mm.rrrr (d je den, m je mesic, r je rok): ";
			//budeme uzivateli verit ze zada rok ve spravnem tvaru. Pokud ne, pak program spadne, protoze se bude
			//zasahovat mimo naalokovanou pamet
			cin >> datum;

			cout << endl;
			cout << "Zadej hodnotu pro elektriku: ";
			cin >> prom_elektrika;
			
			cout << "Zadej hodnotu pro vodu: ";
			cin >> prom_voda;
			
			cout << "Zadej hodnotu pro plyn: ";
			cin >> prom_plyn;
			cout << endl;

			//zapis techto hodnot do textovych souboru + vypsani pro kontrolu do stdout
			zapis_hodnot(datum, prom_elektrika, prom_voda, prom_plyn);
		} //konec if(operace == 1)

		//promenne k operaci 2
		char datum1[11];
		char datum2[11];

		if(operace == 2)
		{
			zpracovani_hodnot(datum, datum1, datum2, &prom_elektrika, &prom_voda, &prom_plyn);
		} //konec if(operace == 2)

		//--------------------------------------vypis do souboru HTML--------------------------------------
		FILE *soubor_html;
		//otevreni souboru
		soubor_html = fopen("energie.html", "w");

		//kontrola, jestli se podarilo soubor spravne otevrit
		if (soubor_html == NULL)
		{
			cout << "chyba pri otvirani souboru energie.html" << endl;
			//je potreba pred skonceni uvolnit pamet
			free(datum);
			exit(EXIT_FAILURE);
		}

		if(operace == 1)
		{
			vypis_1(soubor_html, datum, prom_elektrika, prom_voda, prom_plyn);
		}
		
		//uvolneni dynamicky alokovane pameti
		free(datum);

		if(operace == 2)
		{
			vypis_2(soubor_html, datum1, datum2, prom_elektrika, prom_voda, prom_plyn);
		}

		if(operace == 3)
		{
			vypis_3(soubor_html);
		}
		//--------------------------------------vypis do souboru HTML--------------------------------------

		//kdyz je zadan konec pak je hloupost se ptat jestli skoncit...
		if(operace != 4)
		{
			cout << "Chcete skoncit? 1 = ANO, cokoli jinyho = NE" <<endl;
			cout << "Volba: ";
			//muzeme pouzit promennou "operace", protoze do jejiho dalsiho pouziti bude znova inicializovana na spravnou hodnotu
			cin >> operace;
			if(operace == 1)
			{
				operace = 4;
			}
		}
	} //konec while(operace != 4)
	//system("PAUSE");
	return 0;
	
}